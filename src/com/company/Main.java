package com.company;

import java.io.*;


import com.google.gson.Gson;
import org.json.simple.parser.JSONParser;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import java.util.Scanner;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;


class CreditScoreData {
    public String accountID;
    public int score;

    CreditScoreData(){
        //
    }
}





public class Main {


    private static final int NTHREADS = 16;

    public static void main(String[] args) {

        long startTime = System.nanoTime();

        try {

            final Scanner scanner = new Scanner(new File(args[0]));

            final ExecutorService executor = Executors.newFixedThreadPool(NTHREADS);

            final FileWriter file = new FileWriter(args[1]);

            final StringBuffer buf = new StringBuffer();

            while (scanner.hasNextLine()) {
                String line = scanner.nextLine();
                buf.append(line);
            }

            JSONParser parser = new JSONParser();
            final JSONArray data = (JSONArray) parser.parse(buf.toString());


            // Construct credit score output and append to file
            final Gson gson = new Gson();

            file.append("[");

            for (int i =0 ; i < data.size(); i++) {
                final JSONObject accountObj = (JSONObject) data.get(i);
                if (!accountObj.isEmpty()) {
                    Runnable worker = new Runnable() {
                        @Override
                        public void run() {

                            try {

                                JSONObject account = (JSONObject) accountObj.get("loanAccount");

                                String accountId = (String) account.get("id");

                                //Assign credit score
                                int creditScore = (int) (Math.random() * 10 + 1);

                                CreditScoreData creditScoreData = new CreditScoreData();
                                creditScoreData.accountID = accountId;
                                creditScoreData.score = creditScore;

                                file.append(gson.toJson(creditScoreData) + ",\n");


                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                        }
                    };

                    executor.execute(worker);
                }

            }

            executor.shutdown();
            // Wait until all threads are finished
            executor.awaitTermination(Long.MAX_VALUE, TimeUnit.NANOSECONDS);

            file.append("]");
            file.flush();

        } catch (Exception e) {
            e.printStackTrace();
        }

        long endTime   = System.nanoTime();
        long totalTime = endTime - startTime;



        System.out.println("Program time => " + totalTime/1000000000 + "secs");

    }
}
